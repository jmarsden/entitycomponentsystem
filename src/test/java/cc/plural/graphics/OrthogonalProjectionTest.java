/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.graphics;

import cc.plural.ecs.renderer.OrthogonalProjection;
import java.nio.FloatBuffer;
import cc.plural.math.Matrix4f;
import junit.framework.TestCase;
import org.lwjgl.BufferUtils;

/**
 *
 * @author jmarsden
 */
public class OrthogonalProjectionTest extends TestCase {

    public OrthogonalProjectionTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of setMatrix2D method, of class OrthogonalProjection.
     */
    public void testSetMatrix2D() {
        System.out.println("setMatrix2D");
        Matrix4f matrix = Matrix4f.identity();
        OrthogonalProjection instance = new OrthogonalProjection(0, 800, 600, 0, 0, 1);
        
        
        Matrix4f expResult = null;
        Matrix4f result = instance.setMatrix2D(matrix);
        System.out.println(result);
        result.print(4, 4);
    }

    /**
     * Test of setMatrix method, of class OrthogonalProjection.
     */
    public void testSetMatrix() {
        System.out.println("setMatrix");

        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        org.lwjgl.util.vector.Matrix4f referenceMatrix = new org.lwjgl.util.vector.Matrix4f();
        createReferenceOrthogonalMatrix(referenceMatrix);
        referenceMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();
        System.out.println("Reference Array:\r\n" + matrix44Buffer.toString());
        for(int i = 0; i < matrix44Buffer.capacity(); i++) {
            System.out.print("" + matrix44Buffer.get() + " ");
        }
        System.out.println();

        Matrix4f matrix = Matrix4f.identity();
        OrthogonalProjection instance = new OrthogonalProjection(0, 8, 6, 0);
        Matrix4f expResult = null;
        instance.load(matrix);

        //FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        matrix44Buffer.flip();
        matrix.loadColumnMajor(matrix44Buffer);
        matrix44Buffer.flip();

        System.out.println("ECS Array:\r\n" + matrix44Buffer.toString());
        for(int i = 0; i < matrix44Buffer.capacity(); i++) {
            System.out.print("" + matrix44Buffer.get() + " ");
        }
        System.out.println();

        //assertEquals(expResult, result);
    }

    public org.lwjgl.util.vector.Matrix4f createReferenceOrthogonalMatrix(org.lwjgl.util.vector.Matrix4f projectionMatrix) {
        float left = 0;
        float right = 8;
        float top = 0;
        float bottom = 6;
        float near = 0;
        float far = 1;

        projectionMatrix.m00 = 2f / (right - left);
        projectionMatrix.m11 = 2f / (top - bottom);
        projectionMatrix.m22 = 1f / (far - near);
        projectionMatrix.m33 = 1f;

        projectionMatrix.m30 = -((right + left) / (right - left));
        projectionMatrix.m31 = -((top + bottom) / (top - bottom));
        projectionMatrix.m32 = -((near) / (far - near));

        return projectionMatrix;
    }
}
