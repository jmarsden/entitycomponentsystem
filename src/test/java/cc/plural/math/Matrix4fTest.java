/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.math;

import java.nio.FloatBuffer;
import junit.framework.TestCase;
import org.lwjgl.BufferUtils;

/**
 *
 * @author jmarsden
 */
public class Matrix4fTest extends TestCase {

    public Matrix4fTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getData method, of class Matrix4f.
     */
    public void testCorrectMatrixOutput() {
        System.out.println("testCorrectMatrixOutput");
        org.lwjgl.util.vector.Matrix4f projectionMatrix = new org.lwjgl.util.vector.Matrix4f();
        projectionMatrix.m01 = 5;
        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        projectionMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();

        System.out.println("Matrix:\r\n" + projectionMatrix.toString());
        System.out.println("Array:\r\n" + matrix44Buffer.toString());
        for(int i = 0; i < matrix44Buffer.capacity(); i++) {
            System.out.print("" + matrix44Buffer.get() + " ");
        }
        System.out.println();
    }

    public void testMatrixOutput() {
        System.out.println("testMatrixOutput");
        Matrix4f projectionMatrix = Matrix4f.identity();
        projectionMatrix.mat4x4[4] = 5;
        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        projectionMatrix.loadColumnMajor(matrix44Buffer);
        matrix44Buffer.flip();

        System.out.println("Matrix:\r\n" + projectionMatrix.toString());
        System.out.println("Array:\r\n" + matrix44Buffer.toString());
        for(int i = 0; i < matrix44Buffer.capacity(); i++) {
            System.out.print("" + matrix44Buffer.get() + " ");
        }
        System.out.println();
    }
}
