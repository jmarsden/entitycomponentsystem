/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.graphics;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Model {

    public Vertex[] verticies;

    public ByteBuffer verticiesBuffer;

    public int verticiesCount;

    public short[] indices;

    public ByteBuffer indicesBuffer;

    public int indicesCount;

    public int vaoHandle;

    public int vboHandle;

    public int elementHandle;

    public Model() {
        verticies = null;
        indices = null;

        vaoHandle = -1;
        vboHandle = -1;
        elementHandle = -1;
    }

    public void load() {
        verticiesCount = verticies.length;
        verticiesBuffer = BufferUtils.createByteBuffer(verticiesCount * Vertex.STRIDE);
        FloatBuffer verticesFloatBuffer = verticiesBuffer.asFloatBuffer();
        for(int i = 0; i < verticiesCount; i++) {
            verticesFloatBuffer.put(verticies[i].getElements());
        }
        verticesFloatBuffer.flip();

        indicesCount = indices.length;
        indicesBuffer = BufferUtils.createByteBuffer(indicesCount * 2);
        ShortBuffer indiciesShortBuffer = indicesBuffer.asShortBuffer();
        indiciesShortBuffer.put(indices);
        indiciesShortBuffer.flip();

        vaoHandle = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoHandle);

        // Create a new Vertex Buffer Object in memory and select it (bind)
        vboHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboHandle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STREAM_DRAW);

        // Put the position coordinates in attribute list 0
        GL20.glVertexAttribPointer(0, Vertex.POSITION_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.POSITION_BYTE_OFFSET);
        // Put the color components in attribute list 1
        GL20.glVertexAttribPointer(1, Vertex.COLOR_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.COLOR_BYTE_OFFSET);
        // Put the texture coordinates in attribute list 2
        GL20.glVertexAttribPointer(2, Vertex.TEXTURE_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.TEXTURE_BYTE_OFFSET);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);

        // Create a new VBO for the indices and select it (bind) - INDICES
        elementHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboHandle);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, elementHandle, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public void clean() {
    }

    public boolean valid() {
        return false;
    }
}
