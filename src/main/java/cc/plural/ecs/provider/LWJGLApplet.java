/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.provider;

import cc.plural.ecs.runtime.ApplicationInterface;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.LWJGLUtil;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class LWJGLApplet extends Applet {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = -7690657155781766011L;
    /**
     * The Canvas where the LWJGL Display is added
     */
    Canvas display_parent;
    /**
     * Thread which runs the main game loop
     */
    Thread gameThread;
    /**
     * is the game loop running
     */
    boolean running;
    boolean keyDown;
    long lastFrame;
    int fps;
    long lastFPS;
    ApplicationInterface applicationInterface;
    LWJGLApplicationConfiguration applicationConfiguration;
    LWJGLRenderer renderer;

    public LWJGLApplet(ApplicationInterface applicationInterface, LWJGLApplicationConfiguration applicationConfiguration) {
        if (applicationInterface == null) {
            throw new NullPointerException("applicationInterface cannot be null.");
        }
        if (applicationConfiguration == null) {
            throw new NullPointerException("applicationConfiguration cannot be null.");
        }
        this.applicationInterface = applicationInterface;
        this.applicationConfiguration = applicationConfiguration;

        LWJGLDisplay display = new LWJGLDisplay();
        display.init(applicationConfiguration.width, applicationConfiguration.height);
        display.setTitle(applicationConfiguration.title);
        display.setResizable(applicationConfiguration.resizable);

        renderer = (LWJGLRenderer) display.createRenderer(applicationConfiguration);
        renderer.dumpState();
    }

    /**
     * Once the Canvas is created its add notify method will call this method to
     * start the LWJGL Display and game loop in another thread.
     */
    public void startLWJGL() {
        gameThread = new Thread() {
            @Override
            public void run() {
                running = true;
                try {
                    Display.setParent(display_parent);
                    // Display.setVSyncEnabled(true);
                    Display.create();
                    initGL();
                } catch (LWJGLException e) {
                    e.printStackTrace();
                }
                gameLoop();
            }
        };
        gameThread.start();
    }

    /**
     * Tell game loop to stop running, after which the LWJGL Display will be
     * destoryed. The main thread will wait for the Display.destroy() to
     * complete
     */
    private void stopLWJGL() {
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void start() {
    }

    public void stop() {
        stopLWJGL();
    }

    /**
     * Applet Destroy method will remove the canvas, before canvas is destroyed
     * it will notify stopLWJGL() to stop main game loop and to destroy the
     * Display
     */
    public void destroy() {
        remove(display_parent);
        super.destroy();
        System.out.println("Clear up");
    }

    /**
     * initialise applet by adding a canvas to it, this canvas will start the
     * LWJGL Display and game loop in another thread. It will also stop the game
     * loop and destroy the display on canvas removal when applet is destroyed.
     */
    public void init() {
        setLayout(new BorderLayout());
        try {
            display_parent = new Canvas() {
                /**
                 * Serial UID
                 */
                private static final long serialVersionUID = 4501079082530434024L;

                public void addNotify() {
                    super.addNotify();
                    startLWJGL();
                }

                public void removeNotify() {
                    stopLWJGL();
                    super.removeNotify();
                }
            };
            display_parent.setSize(getWidth(), getHeight());
            add(display_parent);
            display_parent.setFocusable(true);
            display_parent.requestFocus();
            display_parent.setIgnoreRepaint(true);
            // setResizable(true);
            setVisible(true);
        } catch (Exception e) {
            System.err.println(e);
            throw new RuntimeException("Unable to create display");
        }
    }

    public void gameLoop() {
        //long startTime = System.currentTimeMillis() + 5000;
        //long fps = 0;

        applicationInterface.init(renderer);
        applicationInterface.start();

        getDelta();
        lastFPS = getTime();

        while (running) {
            float delta = getDelta();

            applicationInterface.update(delta);

            updateFPS();

            drawLoop();

            Display.update();
            Display.sync(60);

            // F Key Pressed (i.e. released)
            if (keyDown && !Keyboard.isKeyDown(Keyboard.KEY_F)) {
                keyDown = false;

                try {
                    if (Display.isFullscreen()) {
                        Display.setFullscreen(false);
                    } else {
                        Display.setFullscreen(true);
                    }
                } catch (LWJGLException e) {
                    e.printStackTrace();
                }
            }
        }

        applicationInterface.exit();

        Display.destroy();
    }

    public void drawLoop() {
        GL11.glClearColor(applicationConfiguration.backGroundColor.getRed(), applicationConfiguration.backGroundColor.getBlue(), applicationConfiguration.backGroundColor.getGreen(), applicationConfiguration.backGroundColor.getAlpha());
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        GL11.glPushMatrix();

        applicationInterface.render(renderer);

        GL11.glPopMatrix();
    }

    protected void initGL() {
        try {
            System.err.println("LWJGL: " + Sys.getVersion() + " / " + LWJGLUtil.getPlatformName());
            System.err.println("GL_VENDOR: " + GL11.glGetString(GL11.GL_VENDOR));
            System.err.println("GL_RENDERER: " + GL11.glGetString(GL11.GL_RENDERER));
            System.err.println("GL_VERSION: " + GL11.glGetString(GL11.GL_VERSION));
        } catch (Exception e) {
            System.err.println(e);
            running = false;
        }

        Display.setTitle(applicationConfiguration.title);

        GL11.glEnable(GL11.GL_TEXTURE_2D);

        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // enable alpha blending
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        GL11.glViewport(0, 0, display_parent.getWidth(), display_parent.getHeight());

        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, display_parent.getWidth(), display_parent.getHeight(), 0, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
    }

    /**
     * Calculate how many milliseconds have passed since last frame.
     *
     * @return milliseconds passed since last frame
     */
    public float getDelta() {
        long time = getTime();
        float delta = (float) (time - lastFrame);
        lastFrame = time;
        return delta;
    }

    /**
     * Get the accurate system time
     *
     * @return The system time in milliseconds
     */
    public long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    public void updateFPS() {
        if (getTime() - lastFPS > 1000) {
            applicationInterface.fpsUpdateCallback(fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }
}