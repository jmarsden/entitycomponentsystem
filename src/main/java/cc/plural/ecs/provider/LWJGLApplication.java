/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.provider;

import cc.plural.ecs.runtime.ApplicationInterface;
import org.lwjgl.Sys;

public class LWJGLApplication {

    long lastFrame;
    int fps;
    long lastFPS;
    boolean pauseFlag;
    ApplicationInterface applicationInterface;
    LWJGLApplicationConfiguration applicationConfiguration;
    LWJGLRenderer renderer;

    public LWJGLApplication(ApplicationInterface applicationInterface, LWJGLApplicationConfiguration applicationConfiguration) {
        if (applicationInterface == null) {
            throw new NullPointerException("applicationInterface cannot be null.");
        }
        if (applicationConfiguration == null) {
            throw new NullPointerException("applicationConfiguration cannot be null.");
        }
        this.applicationInterface = applicationInterface;
        this.applicationConfiguration = applicationConfiguration;
        pauseFlag = false;

//        String javaNativeProp = System.getProperty("java.library.path");
//        String lwjglProp = System.getProperty("org.lwjgl.librarypath");
//        if (javaNativeProp != null && lwjglProp == null) {
//            String pathSeparator = System.getProperty("path.separator");
//            String property = new File("natives").getAbsolutePath();
//            System.setProperty("java.library.path", javaNativeProp + pathSeparator + property);
//            System.out.println("Found library (" + System.getProperty("java.library.path") + ":" + System.getProperty("org.lwjgl.librarypath") + ")!");
//        } else if (javaNativeProp == null && lwjglProp == null) {
//            String property = new File("natives").getAbsolutePath();
//            System.setProperty("java.library.path", property);
//            System.setProperty("org.lwjgl.librarypath", property);
//            System.out.println("Did not find library (" + System.getProperty("java.library.path") + ":" + System.getProperty("org.lwjgl.librarypath") + ")! setting it to: " + property);
//        } else {
//            System.out.println("Found library (" + System.getProperty("java.library.path") + ":" + System.getProperty("org.lwjgl.librarypath") + ")!");
//        }
    }

    public void start() {
        int width = applicationConfiguration.width;
        int height = applicationConfiguration.height;
        LWJGLDisplay display = new LWJGLDisplay();
        display.setTitle(applicationConfiguration.title);
        display.setResizable(applicationConfiguration.resizable);
        display.init(width, height);

        renderer = (LWJGLRenderer) display.createRenderer(applicationConfiguration);
        renderer.backgroundColor = applicationConfiguration.backGroundColor;
        renderer.init();

        applicationInterface.reSizeCallback(width, height);
        applicationInterface.init(renderer);
        applicationInterface.start();

        getDelta();
        lastFPS = getTime();

        while (!renderer.display.isCloseRequested()) {
            float delta = getDelta();

            applicationInterface.update(delta);

            if (!renderer.display.isActive()) {
                if (!pauseFlag) {
                    applicationInterface.pause();
                    pauseFlag = true;
                }
            } else {
                if (pauseFlag) {
                    applicationInterface.resume();
                    pauseFlag = false;
                }

                int newWidth = renderer.display.getWidth();
                int newHeight = renderer.display.getHeight();
                if (width != newWidth || height != newHeight) {
                    width = newWidth;
                    height = newHeight;
                    this.renderer.display.reSize(width, height);
                    applicationInterface.reSizeCallback(width, height);
                }
                applicationInterface.render(renderer);
            }
            renderer.display.sync(renderer.frameRate);
            renderer.display.update();
            

            updateFPS();
        }

        applicationInterface.exit();
        renderer.display.destroy();
    }

    /**
     * Calculate how many milliseconds have passed since last frame.
     *
     * @return milliseconds passed since last frame
     */
    public float getDelta() {

        long time = getTime();
        float delta = (float) (time - lastFrame);
        lastFrame = time;
        return delta;
    }

    /**
     * Get the accurate system time
     *
     * @return The system time in milliseconds
     */
    public long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    public void updateFPS() {
        if (getTime() - lastFPS > 1000) {
            applicationInterface.fpsUpdateCallback(fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }
}
