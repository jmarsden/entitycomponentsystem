/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.provider;

import cc.plural.ecs.renderer.Extensions;
import cc.plural.ecs.renderer.GLRenderer;
import cc.plural.ecs.renderer.GLSLVersion;
import cc.plural.ecs.renderer.GLVendor;
import cc.plural.ecs.renderer.GLVersion;
import cc.plural.ecs.renderer.JavaVersion;
import cc.plural.ecs.renderer.Platform;
import cc.plural.ecs.renderer.Provider;
import cc.plural.ecs.renderer.Renderer;
import cc.plural.ecs.runtime.ApplicationConfiguration;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.LWJGLUtil;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author jmarsden
 */
public class LWJGLDisplay implements cc.plural.ecs.renderer.Display {

    public boolean initalised;
    public JavaVersion javaVersion;
    public Platform platform;
    public Provider provider;
    public GLVendor glVendor;
    public GLRenderer glRenderer;
    public GLVersion glVersion;
    public GLSLVersion glslVersion;
    public Extensions extensions;
    public boolean displayCreated;
    DisplayMode displayMode;

    public LWJGLDisplay() {
        displayCreated = false;
    }

    public void init(int width, int height) {
        try {
            setDisplayMode(width, height);
            create();
        } catch (LWJGLException ex) {
            Logger.getLogger(LWJGLRendererGL2.class.getName()).log(Level.SEVERE, null, ex);
        }
        setVSyncEnabled(true);
        GL11.glViewport(0, 0, width, height);

        javaVersion = new JavaVersion(System.getProperty("java.version"));
        provider = new Provider("LWJGL", Sys.getVersion());

        int platformID = LWJGLUtil.getPlatform();
        switch (platformID) {
            case LWJGLUtil.PLATFORM_LINUX:
                platform = new Platform(Platform.LINUX, Platform.LINUX_NAME);
                break;
            case LWJGLUtil.PLATFORM_MACOSX:
                platform = new Platform(Platform.OSX, Platform.OSX_NAME);
                break;
            case LWJGLUtil.PLATFORM_WINDOWS:
                platform = new Platform(Platform.WINDOWS, Platform.WINDOWS_NAME);
                break;
        }

        glVendor = new GLVendor(GL11.glGetString(GL11.GL_VENDOR));
        glRenderer = new GLRenderer(GL11.glGetString(GL11.GL_RENDERER));

        String versionString = GL11.glGetString(GL11.GL_VERSION);
        String versionStringSuffix = versionString.substring(0, 2);
        double versionDouble = Double.parseDouble(versionStringSuffix);
        if (versionDouble == 1.1D) {
            glVersion = GLVersion.GL11;
            glslVersion = GLSLVersion.GLSLNONE;
        } else if (versionDouble == 1.2D) {
            glVersion = GLVersion.GL12;
            glslVersion = GLSLVersion.GLSLNONE;
        } else if (versionDouble == 1.3D) {
            glVersion = GLVersion.GL13;
            glslVersion = GLSLVersion.GLSLNONE;
        } else if (versionDouble == 1.4D) {
            glVersion = GLVersion.GL14;
            glslVersion = GLSLVersion.GLSLNONE;
        } else if (versionDouble == 1.5D) {
            glVersion = GLVersion.GL15;
            glslVersion = GLSLVersion.GLSLNONE;
        } else if (versionDouble == 2.0D) {
            glVersion = GLVersion.GL20;
            glslVersion = GLSLVersion.GLSL110;
        } else if (versionDouble == 2.1D) {
            glVersion = GLVersion.GL21;
            glslVersion = GLSLVersion.GLSL120;
        } else if (versionDouble == 3.0D) {
            glVersion = GLVersion.GL30;
            glslVersion = GLSLVersion.GLSL130;
        } else if (versionDouble == 3.1D) {
            glVersion = GLVersion.GL31;
            glslVersion = GLSLVersion.GLSL140;
        } else if (versionDouble == 3.2D) {
            glVersion = GLVersion.GL32;
            glslVersion = GLSLVersion.GLSL150;
        } else if (versionDouble == 3.3D) {
            glVersion = GLVersion.GL33;
            glslVersion = GLSLVersion.GLSL330;
        } else if (versionDouble == 4.0D) {
            glVersion = GLVersion.GL40;
            glslVersion = GLSLVersion.GLSL400;
        } else if (versionDouble == 4.1D) {
            glVersion = GLVersion.GL41;
            glslVersion = GLSLVersion.GLSL410;
        } else if (versionDouble == 4.2D) {
            glVersion = GLVersion.GL42;
            glslVersion = GLSLVersion.GLSL420;
        } else if (versionDouble == 4.3D) {
            glVersion = GLVersion.GL43;
            glslVersion = GLSLVersion.GLSL430;
        } else {
            glVersion = GLVersion.GLHMMN;
            glslVersion = GLSLVersion.GLSLNONE;
        }


        String glExtensions = GL11.glGetString(GL11.GL_EXTENSIONS);
       
        
        extensions = new Extensions(glExtensions);
        
        displayCreated = true;
    }

    public void reSize(int width, int height) {
//        try {
//            setDisplayMode(width, height);
//        } catch (LWJGLException ex) {
//            Logger.getLogger(LWJGLRenderer.class.getName()).log(Level.SEVERE, null, ex);
//        }
        GL11.glViewport(0, 0, width, height);
    }

    public Renderer createRenderer(ApplicationConfiguration configuration) {
        if (!displayCreated) {
            throw new RuntimeException("Must init");
        }
        GLVersion version;
        if(configuration.forceGLVersion != null) {
            version = configuration.forceGLVersion;
        } else {
            version = glVersion;
        }
        if(version.getVersion() >= 1.5 && version.getVersion() < 2.0) {
            return new LWJGLRendererGL15(this);
        } else if (version.getVersion() >= 2.0) {
            return new LWJGLRendererGL2(this);
        } else {
            throw new RuntimeException("You must support OpenGL 2.0 Or Higher");
        }
    }

    public void setDisplayMode(int width, int height) throws LWJGLException {
        displayMode = new DisplayMode(width, height);
        Display.setDisplayMode(displayMode);
    }

    public void create() throws LWJGLException {
        Display.create();
        displayCreated = true;
    }

    public void setTitle(String title) {
        Display.setTitle(title);
    }

    public void setResizable(boolean resizable) {
        Display.setResizable(resizable);
    }

    public boolean isCloseRequested() {
        return Display.isCloseRequested();
    }

    public boolean isActive() {
        return Display.isActive();
    }

    public void update() {
        Display.update(true);
    }

    public void sync(int fps) {
        Display.sync(fps);
    }

    public void setVSyncEnabled(boolean sync) {
        Display.setVSyncEnabled(sync);
    }

    public boolean wasResized() {
        return Display.wasResized();
    }

    public int getWidth() {
        return Display.getWidth();
    }

    public int getHeight() {
        return Display.getHeight();
    }

    public void destroy() {
        Display.destroy();
    }
}
