/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.ecs.provider;

import cc.plural.ecs.component.spatial.SpatialComponent;
import cc.plural.ecs.engine.GameObject;
import cc.plural.ecs.renderer.Camera;
import cc.plural.ecs.renderer.Extensions;
import cc.plural.ecs.renderer.GLSLVersion;
import cc.plural.ecs.renderer.GLVendor;
import cc.plural.ecs.renderer.GLVersion;
import cc.plural.ecs.renderer.Mesh;
import cc.plural.ecs.renderer.OrthogonalProjection;
import cc.plural.ecs.renderer.Platform;
import cc.plural.ecs.renderer.Projection;
import cc.plural.ecs.renderer.Provider;
import cc.plural.ecs.renderer.RenderBatch;
import cc.plural.ecs.renderer.RenderState;
import cc.plural.ecs.renderer.Renderer;
import cc.plural.ecs.renderer.Shader;
import cc.plural.ecs.renderer.SimpleCamera;
import cc.plural.ecs.renderer.Texture;
import cc.plural.graphics.Vertex;
import cc.plural.math.Matrix4f;
import cc.plural.utils.Color;
import java.nio.FloatBuffer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;

/**
 *
 * @author John
 */
public abstract class LWJGLRenderer implements Renderer {

    public Logger logger;
    public Projection projection;
    public final Camera camera;
    public Color backgroundColor;
    public StringLWJGLShader defaultShader;
    public final Matrix4f projectionMatrix;
    public final Matrix4f viewMatrix;
    public final Matrix4f modelMatrix;
    public final LWJGLDisplay display;
    public int frameRate;
    protected boolean lock;
    protected final RenderBatch batch;
    protected boolean skipRender;
    protected FloatBuffer mat4x4Buffer = null;

    public LWJGLRenderer(LWJGLDisplay display) {
        backgroundColor = Color.LIGHT_GREY;

        defaultShader = null;

        projectionMatrix = Matrix4f.identity();
        viewMatrix = Matrix4f.identity();
        modelMatrix = Matrix4f.identity();

        mat4x4Buffer = BufferUtils.createFloatBuffer(16);

        this.display = display;
        this.frameRate = 60;

        this.camera = new SimpleCamera();

        lock = false;
        batch = new RenderBatch();

        logger = LogManager.getLogger(LWJGLRenderer.class);
    }

    public abstract void init();

    public void setOrthoProjection(float left, float right, float top, float bottom, float near, float far) {
        projection = new OrthogonalProjection(left, right, top, bottom, near, far);
        projection.load(projectionMatrix);
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public GLVersion getGLVersion() {
        if (display != null) {
            return display.glVersion;
        } else {
            return null;
        }
    }

    public GLSLVersion getGLSLVersion() {
        if (display != null) {
            return display.glslVersion;
        } else {
            return null;
        }
    }

    public GLVendor getGLVendor() {
        if (display != null) {
            return display.glVendor;
        } else {
            return null;
        }
    }

    public Platform getPlatform() {
        if (display != null) {
            return display.platform;
        } else {
            return null;
        }
    }

    public Provider getProvider() {
        if (display != null) {
            return display.provider;
        } else {
            return null;
        }
    }

    public Extensions getExtensions() {
        if (display != null) {
            return display.extensions;
        } else {
            return null;
        }
    }

    public Projection getProjection() {
        return projection;
    }

    public LWJGLDisplay getDisplay() {
        return display;
    }

    public Camera getCamera() {
        return camera;
    }

    public abstract Shader createShader(String vertexSource, String fragmentSource);

    public abstract void releaseShader(Shader shader);

    public abstract Shader createDefaultShader(String vertexSource, String fragmentSource);

    public abstract Shader getDefaultShader();

    public Texture createTexture(String path) {
        return new FileLWJGLTexture(path);
    }

    public void releaseTexture(Texture texture) {
        texture.release();
    }

    public Mesh createStaticMesh(Vertex[] vertices, short[] indicies) {
        LWJGLInMemoryMeshVAO mesh = new LWJGLInMemoryMeshVAO(vertices, indicies);
        return mesh;
    }
    
    public Mesh createStaticMesh(Shader shader, Vertex[] vertices, short[] indicies) {
        LWJGLInMemoryMeshVAO mesh = new LWJGLInMemoryMeshVAO(vertices, indicies);
        return mesh;
    }

    public void releaseMesh(Mesh mesh) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }

    public void setVSyncEnabled(boolean vSync) {
        this.display.setVSyncEnabled(vSync);
    }

    public void startRender() {
        synchronized (this) {
            lock = true;
        }
    }

    public void addToRenderQueue(GameObject object) {
        if (lock == true) {
            batch.queue.add(object);
        } else {
            throw new RuntimeException("Render Not Started.");
        }
    }

    public boolean isRendering() {
        return lock;
    }

    public abstract void completeRender();

    public boolean cull(SpatialComponent spatial) {
        return false;
    }

    public void setRenderState(RenderState renderState) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Runtime Details").append('\n');
        builder.append("Java Runtime:\t").append(display.javaVersion).append('\n');

        builder.append("Platform:\t").append(display.platform).append('\n');

        builder.append("Graphics:\t").append(display.provider).append('\n');
        builder.append("GLVendor:\t").append(display.glVendor).append('\n');
        builder.append("GLRenderer:\t").append(display.glRenderer).append('\n');
        builder.append("GLVersion:\t").append(display.glVersion).append('\n');
        builder.append("GLSLVersion:\t").append(display.glslVersion).append('\n');
        builder.append("GLExtensions:\t").append(display.extensions).append('\n');

        builder.append("Handler:\t").append(getClass().getCanonicalName()).append('\n');

        return builder.toString();
    }

    void dumpState() {
        System.out.println(toString());
    }

    public void skipRender() {
        skipRender = true;
    }
}
