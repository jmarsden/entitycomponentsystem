package cc.plural.ecs.provider;

import cc.plural.ecs.engine.GameObject;
import cc.plural.ecs.renderer.Shader;
import java.net.URL;
import org.apache.log4j.LogManager;
import org.lwjgl.opengl.GL11;

public class LWJGLRendererGL15 extends LWJGLRenderer {

    public LWJGLRendererGL15(LWJGLDisplay display) {
        super(display);
        logger = LogManager.getLogger(LWJGLRendererGL2.class);
    }

    @Override
    public void init() {
        GL11.glClearColor(backgroundColor.getRed() / 255F, backgroundColor.getGreen() / 255F, backgroundColor.getBlue() / 255F, 1.0f);

    }

    @Override
    public Shader createShader(String vertexSource, String fragmentSource) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    public Shader createShader(URL vertexURL, URL fragmentURL) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public void releaseShader(Shader shader) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public Shader createDefaultShader(String vertexSource, String fragmentSource) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }
    
    @Override
    public Shader createDefaultShader(URL vertexURL, URL fragmentURL) {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public Shader getDefaultShader() {
        throw new UnsupportedOperationException("Not supported on 1.5");
    }

    @Override
    public void completeRender() {
        synchronized (this) {
            if (skipRender) {
                batch.clear();
                lock = false;
                return;
            }

            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            for (GameObject gameObject : batch.queue) {

                if (gameObject.geometry == null || !gameObject.geometry.isValid()) {
                    continue;
                }
                if (gameObject.spatial == null) {
                    continue;
                }

            }
        }
    }
}
