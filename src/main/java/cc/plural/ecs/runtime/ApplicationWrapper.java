/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.runtime;

import cc.plural.ecs.provider.LWJGLApplication;
import cc.plural.ecs.provider.LWJGLApplicationConfiguration;
import cc.plural.utils.Color;
import org.lwjgl.LWJGLException;

public class ApplicationWrapper {

    public static void main(String[] args) throws LWJGLException {
        LWJGLApplicationConfiguration applicationConfiguration = new LWJGLApplicationConfiguration();
        applicationConfiguration.title = "ECS Prototype 3";
        applicationConfiguration.width = 960;
        applicationConfiguration.height = 600;
        applicationConfiguration.resizable = true;
        applicationConfiguration.backGroundColor = Color.LIGHT_GREY;
        LWJGLApplication application = new LWJGLApplication(new RuntimeApplication(), applicationConfiguration);
        application.start();
    }
}
