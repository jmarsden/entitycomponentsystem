package cc.plural.ecs.runtime;

import cc.plural.ecs.component.input.MouseListenerComponentMoveIntent;
import cc.plural.ecs.component.spatial.SpatialComponent;
import cc.plural.ecs.engine.Component;
import cc.plural.ecs.engine.ComponentIntent;

public class PlayerTurretComponent extends Component {

    private float turnSpeed;
    private float xStore;

    public PlayerTurretComponent() {
        turnSpeed = 0;
        xStore = -1;
    }

    @Override
    public void start() {
        turnSpeed = 0.01F;
        xStore = -1;
    }

    @Override
    public void update(float delta) {
        SpatialComponent position = (SpatialComponent) gameObject.getComponent(SpatialComponent.class);
        if (position == null) {
            return;
        }
        /*
         float mouseXPosition = Mouse.getX();
         if(xStore == -1) {
         xStore = mouseXPosition;
         } else {
         position.setRotation((mouseXPosition - xStore) * turnSpeed);
         }
         */
    }

    @Override
    public void recieveIntent(ComponentIntent intent) {
        if (intent.getClass() == MouseListenerComponentMoveIntent.class) {
            MouseListenerComponentMoveIntent mouseMoveIntent = (MouseListenerComponentMoveIntent) intent;
            float x = mouseMoveIntent.getX();
            float y = mouseMoveIntent.getY();

            SpatialComponent position = (SpatialComponent) gameObject.getComponent(SpatialComponent.class);
            if (position == null) {
                return;
            }


        }

        System.out.println(intent);
    }
}
