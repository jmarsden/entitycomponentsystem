/**
 * Copyright (C) 2013 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.runtime;

import cc.plural.ecs.component.reporter.ReporterComponent;
import cc.plural.ecs.engine.Engine;
import cc.plural.ecs.engine.GameObject;
import cc.plural.ecs.engine.Node;
import cc.plural.ecs.renderer.OrthogonalProjection;
import cc.plural.ecs.renderer.Renderer;
import cc.plural.ecs.renderer.Shader;
import cc.plural.utils.MatrixPrinter;
import cc.plural.utils.TrueTypeFont;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RuntimeApplication implements ApplicationInterface {

    int width;
    int height;
    //OrthogonalProjection projection;
    Engine engine;
    TrueTypeFont font;
    int fps;
    Logger logger;

    public void init(Renderer renderer) {
        fps = 0;

        logger = LogManager.getLogger(RuntimeApplication.class);
        if (logger.isInfoEnabled()) {
            logger.info("init()");
        }
        String rendererDump = renderer.toString();
        if (logger.isInfoEnabled()) {
            logger.info(rendererDump);
        }
        Shader shader = renderer.getDefaultShader();
        String shaderDump = shader.getState();
        if (logger.isInfoEnabled()) {
            logger.info(shaderDump);
        }
        
        float left = 0;
        float right = 40;
        float top = 40;
        float bottom = 0;
        float near = 0;
        float far = 1;
        renderer.setOrthoProjection(left, right, top, bottom, near, far);

        float[] mat4x4 = renderer.getProjection().mat4x4;
        System.out.println(MatrixPrinter.matrix2String(MatrixPrinter.MAJOR.ROW, mat4x4));
        
        mat4x4 = renderer.getCamera().get();
        System.out.println(MatrixPrinter.matrix2String(MatrixPrinter.MAJOR.ROW, mat4x4));
        
        engine = new Engine();

        GameObject tank = TankFactory.createTank(engine);
        tank.registerComponent(new ReporterComponent());
        tank.setPosition(20, 20);
        tank.setScale(2);
        tank.updateWorldTransforms();
        
        tank.getWorld().load(mat4x4);
        System.out.println(MatrixPrinter.matrix2String(MatrixPrinter.MAJOR.ROW, mat4x4));

//        Node node1 = engine.createNode("Node 1");
//        Node node2 = engine.createNode("Node 2");
//        Node node3 = engine.createNode("Node 3");
//        Node node4 = engine.createNode("Node 4");
//
//        node1.addChild(node2);
//        node1.addChild(node3);
//        node2.addChild(node4);
//
//        GameObject tank2 = TankFactory.createTank(engine);
//        node4.addChild(tank2);

        engine.init();
    }

    public void start() {
        if (logger.isInfoEnabled()) {
            logger.info(engine.getState());
            logger.info("start()");
        }
        engine.start();
    }

    public void update(float delta) {
        engine.update(delta);
    }

    public void render(Renderer renderer) {
        engine.render(renderer);
    }

    public void pause() {
        if (logger.isInfoEnabled()) {
            logger.info("pause()");
        }
    }

    public void resume() {
        if (logger.isInfoEnabled()) {
            logger.info("resume()");
        }
    }

    public void reSizeCallback(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void fpsUpdateCallback(int fps) {
        this.fps = fps;
    }

    public void exit() {
        if (logger.isInfoEnabled()) {
            logger.info("exit()");
        }
    }
}
