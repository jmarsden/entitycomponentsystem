package cc.plural.ecs.runtime;

import cc.plural.ecs.component.geometry.GeometryComponent;
import cc.plural.ecs.engine.Engine;
import cc.plural.ecs.engine.GameObject;
import cc.plural.graphics.Model;
import cc.plural.graphics.ShaderProgram;
import cc.plural.graphics.Texture;
import cc.plural.graphics.Vertex;

public class TankFactory {

    public static GameObject createTank(Engine engine) {
        GameObject tank = engine.createGameObject();
        tank.setName("Tank");

        GameObject tankBody = engine.createGameObject();
        GeometryComponent geometry = new GeometryComponent();

        //geometry.texture = Texture.createTexture("/images/tank_body_1.png");
//        geometry.model = new Model();
//        VertexData v0 = new VertexData();
//        v0.setXYZ(-0.5f, 0.5f, 0);
//        v0.setRGB(1, 0, 0);
//        v0.setST(0, 0);
//        VertexData v1 = new VertexData();
//        v1.setXYZ(-0.5f, -0.5f, 0);
//        v1.setRGB(0, 1, 0);
//        v1.setST(0, 1);
//        VertexData v2 = new VertexData();
//        v2.setXYZ(0.5f, -0.5f, 0);
//        v2.setRGB(0, 0, 1);
//        v2.setST(1, 1);
//        VertexData v3 = new VertexData();
//        v3.setXYZ(0.5f, 0.5f, 0);
//        v3.setRGB(1, 1, 1);
//        v3.setST(1, 0);
//        geometry.model.verticies = new VertexData[] {v0, v1, v2, v3};
//        geometry.model.indices = new short[] {
//            0, 1, 2,
//            2, 3, 0
//        };
//        geometry.model.load();

        //ShaderProgram shader = new ShaderProgram();
        //shader.createDefaultShaderProgram();
        //geometry.shader = shader;

        tankBody.registerComponent(geometry);
        tankBody.setName("TankBody");

        tank.addChild(tankBody);

//		Component playerComponent = new PlayerDriveComponent();		
//		tank.registerComponent(playerComponent);
//		Component mouseListenerComponent = new MouseListenerComponent();
//		tank.registerComponent(mouseListenerComponent);
//		

//		
//		GameObject rightTankTrack = engine.createGameObject();
//		rightTankTrack.setPosition(10, 0);
//		rightTankTrack.registerComponent(new GeometryComponent("images/tank_track_right_2.png"));
//		tank.addChild(rightTankTrack);
//		
//		GameObject leftTankTrack = engine.createGameObject();
//		leftTankTrack.setPosition(-10, 0);
//		leftTankTrack.registerComponent(new GeometryComponent("images/tank_track_left_2.png"));
//		tank.addChild(leftTankTrack);
//
//		GameObject tankTurret = engine.createGameObject();
//		tankTurret.setPosition(0, -3);
//		tankTurret.setRotation(QuickMath.toRadians(45));
//		GeometryComponent turretAppearance = new GeometryComponent("images/tank_turret_1.png");
//		turretAppearance.setYCentreOffset(6);
//		tankTurret.registerComponent(turretAppearance);
//		PlayerTurretComponent turretComponent = new PlayerTurretComponent();
//		tankTurret.registerComponent(turretComponent);
//		//turretComponent.listenToTopic(IntentRegistry.lookupSingleton().lookupIntent(MouseListenerComponentMoveIntent.class));
//		
//		tank.addChild(tankTurret);
//		
//		
        return tank;
    }
}
