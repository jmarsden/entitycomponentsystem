package cc.plural.ecs.engine;

import java.util.ArrayList;
import java.util.List;

public class IntentRegistry {

	final List<Class<? extends ComponentIntent>> registry;
	
	private static IntentRegistry singleton;
	
	static {
		singleton = null;
	}
	
	public IntentRegistry() {
		registry = new ArrayList<Class<? extends ComponentIntent>>();
	}
	
	public int registerIntent(Class<? extends ComponentIntent> intentClass) {
		synchronized(registry) {
			if(registry.contains(intentClass)) {
				return registry.indexOf(intentClass);
			}
			registry.add(intentClass);
			// TODO Can I just use size()-1?
			return registry.indexOf(intentClass);
		}
	}
	
	public int registerIntent(ComponentIntent intent) {
		Class<? extends ComponentIntent> intentClass = intent.getClass();
		return registerIntent(intentClass);
	}
	
	public int lookupIntent(Class<? extends ComponentIntent> intentClass) {
        if(registry.contains(intentClass)) {
            return registry.indexOf(intentClass);
        }
		return registerIntent(intentClass);
	}
	
	public static IntentRegistry lookupSingleton() {
		if(singleton == null) {
			singleton = new IntentRegistry();
		}
		return singleton;
	}
}
