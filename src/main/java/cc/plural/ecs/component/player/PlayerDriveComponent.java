/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.component.player;

import org.lwjgl.input.Keyboard;

import cc.plural.ecs.component.spatial.SpatialComponent;
import cc.plural.ecs.engine.Component;
import cc.plural.math.QuickMath;

public class PlayerDriveComponent extends Component {

	private float moveSpeed;
	private float turnSpeed;
	
	public PlayerDriveComponent() {
		moveSpeed = 0;
		turnSpeed = 0;
	}
	
	@Override
	public void start() {
		moveSpeed = 3F;
		turnSpeed = 0.05F;
	}

	@Override
	public void update(float delta) {
		SpatialComponent position = (SpatialComponent) gameObject.getComponent(SpatialComponent.class);
		if(position == null) {
			return;
		}
		
		synchronized(position) {
//			if(Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S)) {
//				float xPosition = position.getX();
//				float yPosition = position.getY();
//				float cosRotation = QuickMath.cos(position.getRotation());
//				float sinRotation = QuickMath.sin(position.getRotation());
//				float newX = moveSpeed * sinRotation;
//				float newY = -moveSpeed * cosRotation;
//				position.setPosition(xPosition + newX, yPosition + newY);
//				onMoveForward();
//			}
//			if(Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_W)) {
//				float xPosition = position.getX();
//				float yPosition = position.getY();
//				float cosRotation = QuickMath.cos(position.getRotation());
//				float sinRotation = QuickMath.sin(position.getRotation());
//				float newX = -moveSpeed * sinRotation;
//				float newY = moveSpeed * cosRotation;
//				position.setPosition(xPosition + newX, yPosition + newY);
//				onMoveBackwards();
//			}
//			if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
//				position.setRotation(position.getRotation()-turnSpeed);
//				onTurnRight();
//			}
//			if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
//				position.setRotation(position.getRotation()+turnSpeed);
//				onTurnLeft();
//			}	
		}
	}
	
	public void onMoveForward() {
		sendIntent(new PlayerComponentMoveForward(this));
	}
	
	public void onMoveBackwards() {
		sendIntent(new PlayerComponentMoveBackwards(this));
	}
	
	public void onTurnLeft() {
		sendIntent(new PlayerComponentTurnLeft(this));
	}
	
	public void onTurnRight() {
		sendIntent(new PlayerComponentTurnRight(this));
	}
}
