/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.component.input;

import cc.plural.ecs.engine.Component;
import cc.plural.ecs.engine.ComponentIntent;

public class MouseListenerComponentYChangeIntent extends ComponentIntent {
	float y;
	float yDelta;
	
	public MouseListenerComponentYChangeIntent(Component source, float y, float yDelta) {
		super(source);
		this.y = y;
		this.yDelta = yDelta;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * @return the yDelta
	 */
	public float getyDelta() {
		return yDelta;
	}

	/**
	 * @param yDelta the yDelta to set
	 */
	public void setyDelta(float yDelta) {
		this.yDelta = yDelta;
	}	
}
