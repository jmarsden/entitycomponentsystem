/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.component.input;

import cc.plural.ecs.engine.Component;
import org.lwjgl.input.Mouse;

public class MouseListenerComponent extends Component {

	public float xStore;
	public float yStore;
	
	public MouseListenerComponent() {
		xStore = 0;
		yStore = 0;
	}
	
	@Override
	public void start() {
		xStore = Mouse.getX();
		yStore = Mouse.getY();
		onXChange(xStore, 0);
		onYChange(yStore, 0);
		onMove(xStore, yStore);
	}

	@Override
	public void update(float delta) {
		float x = Mouse.getX();
		float y = Mouse.getY();
		if(x != xStore) {
			onXChange(x, xStore-x);
			if(y != yStore) {
				onXChange(y, yStore-y);
			}
			onMove(x, y);
			xStore = x;
			yStore = y;
		} else if(y != yStore) {
			onXChange(y, yStore-y);
			onMove(x, y);
			xStore = x;
			yStore = y;
		}
	}
	
	public void onXChange(float x, float xDelta) {
		sendIntent(new MouseListenerComponentXChangeIntent(this, x, xDelta));
	}
	
	public void onYChange(float y, float yDelta) {
		sendIntent(new MouseListenerComponentYChangeIntent(this, y, yDelta));
	}
	
	public void onMove(float x, float y) {
		sendIntent(new MouseListenerComponentMoveIntent(this, x, y));
	}
}
