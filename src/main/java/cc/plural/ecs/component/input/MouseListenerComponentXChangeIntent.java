/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.component.input;

import cc.plural.ecs.engine.Component;
import cc.plural.ecs.engine.ComponentIntent;

public class MouseListenerComponentXChangeIntent extends ComponentIntent {

	float x;
	float xDelta;
	
	public MouseListenerComponentXChangeIntent(Component source, float x, float xDelta) {
		super(source);
		this.x = x;
		this.xDelta = xDelta;
	}

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return the xDelta
	 */
	public float getxDelta() {
		return xDelta;
	}

	/**
	 * @param xDelta the xDelta to set
	 */
	public void setxDelta(float xDelta) {
		this.xDelta = xDelta;
	}
}
