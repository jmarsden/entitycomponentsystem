/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.ecs.component.reporter;

import cc.plural.ecs.engine.Component;
import cc.plural.ecs.engine.ComponentIntent;
import cc.plural.ecs.renderer.Renderer;

public class ReporterComponent extends Component {

	@Override
	public void start() {
	}

	@Override
	public void update(float delta) {
	}
	
//	@Override
//	public void render(Renderer renderer) {
//	}

	/* (non-Javadoc)
	 * @see cc.plural.engine.Component#isListening()
	 */
	@Override
	public boolean isListening() {
		return true;
	}

	/* (non-Javadoc)
	 * @see cc.plural.engine.Component#listensToTopic(int)
	 */
	@Override
	public boolean listensToTopic(int topic) {
		return true;
	}

	@Override
	public void recieveIntent(ComponentIntent intent) {
		System.out.println("ReporterComponent.recieveIntent(\"" + intent + "\")(Src=\"" + intent.source + "\")");
	}
}
