/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.ecs.renderer;

import cc.plural.ecs.engine.GameObject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jmarsden
 */
public class RenderBatch {
    
    final public List<GameObject> queue;
    
    public RenderBatch() {
        queue = new ArrayList<GameObject>();
    }
    
    public void clear() {
        queue.clear();
    }
}
