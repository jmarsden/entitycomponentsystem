/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.ecs.renderer;

/**
 *
 * @author jmarsden
 */
public class LookAt {
    
    float cameraX;
    float cameraY;
    float cameraZ;
    float focusX;
    float focusY;
    float focusZ;
    float upX;
    float upY;
    float upZ;
    
    public LookAt(float cameraX, float cameraY, float cameraZ,
            float focusX, float focusY, float focusZ,
            float upX, float upY, float upZ) {
        this.cameraX = cameraX;
        this.cameraY = cameraY;
        this.cameraZ = cameraZ;
        this.focusX = focusX;
        this.focusY = focusY;
        this.focusZ = focusZ;
        this.upX = upX;
        this.upY = upY;
        this.upZ = upZ;
    }

    public void get(float[] view44Matrix) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
