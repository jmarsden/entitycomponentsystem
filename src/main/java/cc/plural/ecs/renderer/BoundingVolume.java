/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.ecs.renderer;

import cc.plural.math.Vector3f;

/**
 *
 * @author jmarsden
 */
public class BoundingVolume {

    public Vector3f min;

    public Vector3f max;

    public BoundingVolume() {
        min = new Vector3f();
        max = new Vector3f();
    }

    public void reset() {
        min.x = 0;
        min.y = 0;
        min.z = 0;
        max.x = 0;
        max.y = 0;
        max.z = 0;
    }
}
