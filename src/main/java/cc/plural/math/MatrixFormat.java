/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.math;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 *
 * @author j.w.marsden@gmail.com
 */
public class MatrixFormat extends Format {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = 1678428258443167556L;
    String rowPrefix;
    String rowSuffix;
    String cellSeparator;
    NumberFormat cellFormat;

    public MatrixFormat() {
        rowPrefix = "[";
        rowSuffix = " ]";
        cellSeparator = ", ";
        cellFormat = new DecimalFormat(" ####0.0000;-####0.0000");
    }

    public int[] getColumnCharacterWidths(Matrix a) {
        return null;
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
//        if(obj instanceof ImmutableMatrixD) {
//            ImmutableMatrixD a = (ImmutableMatrixD) obj;
//            double[][] data = a.getData();
//            for(int i=0;i<a.getM();i++) {
//                toAppendTo.append(rowPrefix);
//                for(int j=0;j<a.getN();j++) {
//                    toAppendTo.append(cellFormat.format(data[i][j])).append((j<a.getN()-1) ? cellSeparator : "");
//                }
//                toAppendTo.append(rowSuffix).append("\r\n");
//            }
//        }
        return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static String format(Object obj, String title) {
        MatrixFormat format = new MatrixFormat();
        return String.format("%s:\r\n%s", title, format.format(obj));
    }
}
