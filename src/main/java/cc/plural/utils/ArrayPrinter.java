/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.utils;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 *
 * @author John
 */
public class ArrayPrinter {

    public static void printArray(float[] a, StringBuilder builder) {
        if (a == null) {
            builder.append("null");
        }
        int length = a.length;
        builder.append('[');
        for (int i = 0; i < (length - 1); i++) {
            float v = a[i];
            if (v >= 0) {
                builder.append(' ');
            }
            builder.append(v).append(',');
        }
        if (length > 0) {
            float v = a[length - 1];
            if (v >= 0) {
                builder.append(' ');
            }
            builder.append(v);
        }
        builder.append(']');
    }

    public static void printArray(float[] a) {
        StringBuilder builder = new StringBuilder();
        printArray(a, builder);
        System.out.println(builder.toString());
    }

    public static String arrayToString(ShortBuffer array) {
        StringBuilder output = new StringBuilder();
        if (array == null) {
            output.append("null");
        } else {
            output.append('[');
            array.rewind();
            for (int i = 0; i < array.capacity() - 1; i++) {
                output.append(array.get()).append(',');
            }
            if(array.capacity() > 0) {
                output.append(array.get());
            }
            output.append(']');
        }
        return output.toString();
    }
    
    public static String arrayToString(FloatBuffer array) {
        StringBuilder output = new StringBuilder();
        if (array == null) {
            output.append("null");
        } else {
            output.append('[');
            array.rewind();
            for (int i = 0; i < array.capacity() - 1; i++) {
                output.append(array.get()).append(',');
            }
            if(array.capacity() > 0) {
                output.append(array.get());
            }
            output.append(']');
        }
        return output.toString();
    }
}
